---
title: 'Knives'
weight: 2
header_menu: true
---
That is the important part, right? You want to know what I can do for you. This is why I put this right up there into the header menu of the website.
{{< relref path="/" >}}
[Slovensky](/sk/)

---

## Knive 1

This is not an easy task. You will likely have to pay money for this. You know what - let us look at a nice picture first.

![Nice picture to make you pay me ;-)](images/knives/knive1.png)

Wow. That was nice, right? Well, call me and let us talk.

---

## Knive 2

This is not an easy task. You will likely have to pay money for this. You know what - let us look at a nice picture first.

![Nice picture to make you pay me ;-)](images/knives/knive2.png)

Wow. That was nice, right? Well, call me and let us talk.

---

## Knive 3

This is not an easy task. You will likely have to pay money for this. You know what - let us look at a nice picture first.

![Nice picture to make you pay me ;-)](images/knives/knive3.png)

Wow. That was nice, right? Well, call me and let us talk.
